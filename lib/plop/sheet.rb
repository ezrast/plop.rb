require 'json'

module Plop
  class Sheet
    def initialize(tabs)
      @tabs = tabs
    end

    def to_h(priv: true)
      id = 0
      properties = []

      append = ->(property, rank, parent_id) {
        this_id = (id += 1)
        properties.push({
          id: this_id,
          parentId: parent_id,
          type: property.type,

          data: property.data,
          formula: property.formula,
          message: property.message,
          name: property.name,
          size: property.size,
          value: property.value,

          rank: rank,
          characterId: 1,
        }.compact)
        property.children.each_with_index{ |child, idx| append[child, idx, this_id] }
      }

      @tabs.each_with_index{ |tab, idx| append[tab, idx, nil] }

      return {
        "properties" => properties,
        "private" => priv,
        "type" => "tableplop-character-v2",
      }
    end

    def to_json
      to_h.to_json
    end

    module Property
      attr_reader :children, :data, :type, :formula, :message, :name, :size, :value

      def initialize(children: [], data: nil, type: nil, formula: nil, message: nil, name: nil, size: nil, value: nil)
        @children, @data, @type, @formula, @message, @name, @size, @value = children, data, type, formula, message, name, size, value
      end
    end

    module Properties
      class Ability
        include Property

        def type = "ability"

        def initialize(name, score_value: nil, score_formula: nil, value: nil, message: nil, formula: nil)
          children = if score_value || score_formula
            [Number.new("#{name}-score", score_value, formula: score_formula)]
          else
            []
          end

          super(children: children, name: name, value: value, message: message, formula: formula)
        end
      end

      class Appearance
        include Property

        def type = 'appearance'

        def initialize
          super(data: {appearances: []})
        end
      end

      class Checkbox
        include Property

        def type = 'checkbox'

        def initialize(name, value: false, formula: nil, message: nil)
          super(name: name, value: value, formula: formula, message: message)
        end
      end

      class Checkboxes
        include Property

        def type = 'checkboxes'

        def initialize(name, maximum, value: 0, formula: nil, message: nil)
          child = Number.new("#{name}-max", maximum)
          super(children: [child], name: name, value: value, formula: formula, message: message)
        end
      end

      class Heading
        include Property

        def type = 'heading'

        def initialize(value)
          super(value: value)
        end
      end

      class Health
        include Property

        def type = 'health'

        def initialize(name, maximum, formula: nil)
          children = [
            Number.new("#{name}-maximum", maximum, formula: formula),
            Number.new("#{name}-temporary", 0),
          ]
          super(children: children, name: name)
        end
      end

      class HorizontalSection
        include Property

        def type = 'horizontal-section'

        def initialize(columns)
          specified_size = 0
          unsized_columns = 0
          columns.each do |column|
            unsized_columns += 1 if Array === column
            specified_size += column.size if Section === column
          end

          children = columns.map do |column|
            if Array === column
              size = (100.0 - specified_size) / unsized_columns
              Section.new(size, column)
            else
              column
            end
          end

          super(children: children)
        end
      end

      class Message
        include Property

        def type = 'message'

        def initialize(name, message)
          super(name: name, message: message)
        end
      end

      class Number
        include Property

        def type = 'number'

        def initialize(name, value, formula: nil, message: nil)
          super(name: name, value: value, formula: formula, message: message)
        end
      end

      class Paragraph
        include Property

        def type = 'paragraph'

        def initialize(value)
          super(value: value)
        end
      end

      class Skill
        include Property

        def type = 'save'

        def initialize(name, checked: false, value: 0, message: nil, formula: nil)
          children = [
            Checkbox.new("#{name}-proficiency", value: checked),
          ]
          super(name: name, value: value, message: message, formula: formula, children: children)
        end
      end

      class Section
        include Property

        def type = 'section'

        def initialize(size, children)
          super(size: size, children: children)
        end
      end

      class Skill
        include Property

        def type = 'skill'

        def initialize(name, checked: 0, value: 0, message: nil, formula: nil, subtitle: "")
          children = [
            Checkbox.new("#{name}-proficiency", value: checked.to_i >= 1),
            Checkbox.new("#{name}-expertise", value: checked.to_i >= 2),
          ]
          super(name: name, value: value, message: message, formula: formula, data: {subtitle: subtitle}, children: children)
        end
      end

      class Skill4
        include Property

        def type = 'skill-4'

        def initialize(name, checked: 0, value: 0, message: nil, formula: nil, subtitle: "")
          children = [
            Checkbox.new("#{name}-trained", value: checked.to_i >= 1),
            Checkbox.new("#{name}-expert", value: checked.to_i >= 2),
            Checkbox.new("#{name}-master", value: checked.to_i >= 3),
            Checkbox.new("#{name}-legendary", value: checked.to_i >= 4),
          ]
          super(name: name, value: value, message: message, formula: formula, data: {subtitle: subtitle}, children: children)
        end
      end

      class Tab
        include Property

        def type = 'tab-section'

        def initialize(value, children)
          super(value: value, children: children)
        end
      end

      class Table
        include Property

        def type = 'table'

        def initialize(headers, children)
          super(data: {headers: headers}, children: children)
        end
      end

      class Text
        include Property

        def type = 'text'

        def initialize(name, value, formula: nil, message: nil)
          super(name: name, value: value, formula: formula, message: message)
        end
      end

      class TitleSection
        include Property

        def type = 'title-section'

        def initialize(value, children, collapsed: false)
          super(value: value, children: children, data: {collapsed: collapsed})
        end
      end
    end
  end
end
