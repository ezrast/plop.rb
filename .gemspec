Gem::Specification.new do |s|
  s.name        = 'plop.rb'
  s.version     = '0.1.0'
  s.licenses    = ['Apache-2.0']
  s.summary     = "Utilties for working with Tableplop (https://new.tableplop.com)"
  s.authors     = ["Ezra Stevens"]
  s.email       = 'ezrastevens@cloudandtree.com'
  s.files       = ["lib/sheet.rb"]
  s.homepage    = 'https://gitlab.com/ezrast/plop.rb'
  s.metadata    = { "source_code_uri" => "https://gitlab.com/ezrast/plop.rb" }
end
